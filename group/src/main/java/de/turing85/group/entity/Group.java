package de.turing85.group.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonDeserialize(builder = Group.Builder.class)
public class Group {
  String name;

  @JsonPOJOBuilder(withPrefix = "")
  public static class Builder {
  }
}