package de.turing85.group.jms;

import de.turing85.group.entity.Group;
import io.jaegertracing.internal.JaegerSpanContext;
import io.opentracing.References;
import io.opentracing.Span;
import io.opentracing.Tracer;
import io.opentracing.tag.Tags;
import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.amqp.IncomingAmqpMetadata;
import java.math.BigInteger;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import javax.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.Logger;

@ApplicationScoped
@RequiredArgsConstructor
public class JmsGroupReceiver {

  private final Logger logger;
  private final Tracer tracer;

  @Incoming("jms-new-group-channel")
  CompletionStage<Void> receiveGroup(Message<Group> received) {
    final Span receiveSpan = constructAndStartSendSpan(received);
    // @formatter:off
    return Uni.createFrom().item(received)
        .onItem()
            .transform(Message::getPayload)
            .invoke(group -> logger.info("Received {}", group))
        .subscribe()
            .asCompletionStage()
        .thenRun(receiveSpan::finish)
        .thenRun(received::ack);
    // @formatter:on
  }

  private Span constructAndStartSendSpan(Message<Group> received) {
    final IncomingAmqpMetadata metadata =
        received.getMetadata(IncomingAmqpMetadata.class).orElseThrow();
    final JaegerSpanContext context = createJaegerSpanContextFromMetadata(metadata);
    final String destination = Optional.of(metadata)
        .map(IncomingAmqpMetadata::getAddress)
        .orElse("<UNKNOWN>");
    return buildAndStartReceiveSpan(context, destination);
  }

  private JaegerSpanContext createJaegerSpanContextFromMetadata(IncomingAmqpMetadata metadata) {
    final String uberTraceId = Optional.ofNullable(metadata)
        .map(IncomingAmqpMetadata::getCorrelationId)
        .orElse("");
    final String[] parts = uberTraceId.split(":");
    return new JaegerSpanContext(
        high(parts[0]),
        new BigInteger(parts[0], 16).longValue(),
        new BigInteger(parts[1], 16).longValue(),
        new BigInteger(parts[2], 16).longValue(),
        new BigInteger(parts[3], 16).byteValue());
  }

  private static long high(String hexString) {
    final int hexStringLength = hexString.length();
    return hexStringLength > 16
        ? Long.parseLong(hexString.substring(0, hexStringLength - 16), 16)
        : 0L;
  }

  private Span buildAndStartReceiveSpan(JaegerSpanContext context, String destination) {
    return tracer
        .buildSpan("From_" + destination)
        .withTag(Tags.SPAN_KIND.getKey(), Tags.SPAN_KIND_CONSUMER)
        .withTag(Tags.COMPONENT.getKey(), "java-amqp")
        .withTag(Tags.MESSAGE_BUS_DESTINATION.getKey(), destination)
        .withTag(Tags.PEER_SERVICE.getKey(), "jms")
        .addReference(References.FOLLOWS_FROM, context)
        .start();
  }
}