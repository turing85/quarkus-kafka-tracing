package de.turing85.group.kafka;

import de.turing85.group.entity.Group;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class GroupDeserializer extends ObjectMapperDeserializer<Group> {

  public GroupDeserializer() {
    super(Group.class);
  }
}