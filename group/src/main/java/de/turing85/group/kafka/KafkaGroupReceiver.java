package de.turing85.group.kafka;

import de.turing85.group.entity.Group;
import io.smallrye.mutiny.Uni;
import javax.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;

@ApplicationScoped
@RequiredArgsConstructor
public class KafkaGroupReceiver {

  private final Logger logger;

  @Incoming("kafka-new-group-channel")
  void receiveGroup(Group received) {
    Uni.createFrom().item(received)
        .subscribe().with(group -> logger.info("Received {}", group));
  }
}