# Tracing with JMS and Kafka

## Prerequisite

- Java `<= 11` (version `16` will make problems with lombok)
- a local docker installation

## Starting the docker dependencies:

    cd localdeployments && docker-compose up -d

## Starting the services

In one terminal:

    ./mvnw -pl user quarkus:dev

In a second terminal:

    ./mvnw -pl group quarkus:dev

## Sending a request
We can access the swagger-ui for the user service at 
[`http://localhost:8080/q/swagger-ui`](http://localhost:8080/q/swagger-ui) and send a `POST` request
to the user service to create a new user with an associated group.

## Inspecting traces:

We open [http://localhost:16686](http://localhost:16686). Under "services", we
select
"user-service", scroll down and click "Find Traces". We should see a trace
similar to this one:

![uber-trace](images/ubertrace.png)

We already see three spans in the "user-service" (one for the REST-call, one for
the
`GroupEmitter:send`-method, one for the emitted kafka-message) and one span in
the "group-service". If we click on the span, we see more details:

![span-trace](images/spantrace.png)

In the upper right, hover over "Trace Timeline", then - in the drop-down - click
on "Trace Graph":

![trace-graph](images/tracegraph.png)

## Cleanup

Stop both quarkus-services (go to the two terminals, hit <kbd>CTRL+C</kbd>).
Then, stop the docker-containers:

    cd localdeployment && docker-compose down