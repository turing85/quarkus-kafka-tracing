package de.turing85.user.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value(staticConstructor = "of")
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class User {
  String name;
  Group group;
}