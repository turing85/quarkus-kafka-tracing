package de.turing85.user.entity;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Group {
  String name;
}