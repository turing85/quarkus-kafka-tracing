package de.turing85.user.entity;

import de.turing85.user.boundary.UserRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface UserMapper {
  User toEntity(UserRequest request);
}