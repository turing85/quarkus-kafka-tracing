package de.turing85.user;

import de.turing85.user.boundary.UserRequest;
import de.turing85.user.entity.User;
import de.turing85.user.entity.UserMapper;
import de.turing85.user.jms.JmsGroupEmitter;
import de.turing85.user.kafka.KafkaGroupEmitter;
import io.smallrye.mutiny.Uni;
import java.net.URI;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.Logger;
import org.yaml.snakeyaml.util.UriEncoder;

@Path(UserResource.PATH)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class UserResource {
  static final String PATH = "/users";

  UserMapper userMapper;
  KafkaGroupEmitter kafkaGroupEmitter;
  JmsGroupEmitter jmsGroupEmitter;
  Logger logger;

  @POST
  public Uni<Response> createUser(UserRequest request) {
    // @formatter:off
    return Uni.createFrom().item(request)
        .onItem()
            .transform(userMapper::toEntity)
            .invoke(user -> logger.info("Received {}", user))
            .invoke(this::sendMessage)
        .onItem().transform(user -> Response
            .created(URI.create(UserResource.PATH + "/" + UriEncoder.encode(user.getName())))
            .entity(user)
            .build());
    // @formatter:on
  }

  private void sendMessage(User user) {
    // @formatter:off
    Uni.createFrom().item(user.getGroup())
        .onItem()
            .transform(Message::of)
            .invoke(jmsGroupEmitter::emit)
            .invoke(kafkaGroupEmitter::emit)
        .onItem()
            .transform(Message::getPayload)
        .subscribe().with(group -> logger.info("Sent {}", group));
    // @formatter:on
  }
}