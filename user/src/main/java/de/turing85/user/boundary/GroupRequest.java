package de.turing85.user.boundary;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonDeserialize(builder = GroupRequest.Builder.class)
public class GroupRequest {
  String name;

  @JsonPOJOBuilder(withPrefix = "")
  public static class Builder {
  }
}