package de.turing85.user.boundary;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonDeserialize(builder = UserRequest.Builder.class)
public class UserRequest {
  String name;
  GroupRequest group;

  @JsonPOJOBuilder(withPrefix = "")
  public static class Builder {
  }
}