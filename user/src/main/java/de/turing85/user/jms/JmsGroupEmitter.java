package de.turing85.user.jms;

import de.turing85.user.entity.Group;
import io.jaegertracing.internal.JaegerSpanContext;
import io.opentracing.References;
import io.opentracing.Span;
import io.opentracing.Tracer;
import io.opentracing.tag.Tags;
import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.amqp.OutgoingAmqpMetadata;
import javax.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Message;

@ApplicationScoped
public class JmsGroupEmitter {

  private static final String DESTINATION = "jms-new-group-channel";

  private final Emitter<Group> emitter;
  private final Tracer tracer;

  public JmsGroupEmitter(@Channel(DESTINATION) Emitter<Group> emitter, Tracer tracer) {
    this.emitter = emitter;
    this.tracer = tracer;
  }

  public void emit(Message<Group> message) {
    final Span sendSpan = createAndStartSendSpan();
    // @formatter:off
    Uni.createFrom().item(message)
        .onItem()
            .transform(msg -> addUberTraceId(sendSpan, msg))
            .invoke(emitter::send)
        .subscribe()
            .asCompletionStage()
        .thenRun(sendSpan::finish);
    // @formatter:on
  }

  private Span createAndStartSendSpan() {
    return tracer.buildSpan("To_" + DESTINATION)
        .addReference(References.CHILD_OF, tracer.activeSpan().context())
        .withTag(Tags.SPAN_KIND.getKey(), Tags.SPAN_KIND_PRODUCER)
        .withTag(Tags.COMPONENT.getKey(), "java-amqp")
        .withTag(Tags.MESSAGE_BUS_DESTINATION.getKey(), DESTINATION)
        .withTag(Tags.PEER_SERVICE.getKey(), "jms")
        .start();
  }


  private Message<Group> addUberTraceId(Span sendSpan, Message<Group> msg) {
    return msg.addMetadata(OutgoingAmqpMetadata.builder()
        .withCorrelationId(extractUberTraceId(sendSpan))
        .build());
  }

  private String extractUberTraceId(Span span) {
    final JaegerSpanContext context = ((JaegerSpanContext) span.context());
    return String.format(
        "%s:%s:%s:%s",
        context.getTraceId(),
        Long.toHexString(context.getSpanId()),
        Long.toHexString(context.getParentId()),
        Integer.toHexString(context.getFlags()));
  }
}