package de.turing85.user.kafka;

import de.turing85.user.entity.Group;
import javax.enterprise.context.ApplicationScoped;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Message;

@ApplicationScoped
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class KafkaGroupEmitter {

  Emitter<Group> emitter;

  public KafkaGroupEmitter(@Channel("kafka-new-group-channel") Emitter<Group> emitter) {
    this.emitter = emitter;
  }

  @KafkaBridge
  public void emit(Message<Group> groupMessage) {
    emitter.send(groupMessage);
  }
}